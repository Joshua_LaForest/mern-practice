import logo from './logo.svg';
import './App.css';
import {BrowserRouter, NavLink, Routes, Route } from 'react-router-dom';
import BackendConnector from './BackendConnector';
import HomePage from './HomePage.js';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p className=' font-extrabold text-red-500'>
          Edit <code>src/App.js</code> and save to reload. TESTING
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <BrowserRouter>
        <ul>
        <li>
            <NavLink to="/">
              Home Page
              </NavLink>
              </li>
          <li>
            <NavLink to="/backend-connector">
              Backend connector
            </NavLink>
          </li>
        </ul>
        <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/backend-connector" element = {<BackendConnector />} />
      </Routes>
      </BrowserRouter>
      </header>
      
      
    </div>
    
  );
}

export default App;
