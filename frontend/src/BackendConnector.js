import { useEffect, useState } from "react"

export default function BackendConnector(){
    // let getData = 'something stupid'
    const [getData, setData] = useState([])
    useEffect(() =>{
        async function getBackendData(){
            let url = `${process.env.REACT_APP_MICRO_PRACTICE}items-get`;
            console.log("url: " + url);
            let response = await fetch(url);
            console.log("response: " + response);
            let returnData = await response.json();
            console.log("data: " + returnData[0]);
            if(response.ok){
                setData(returnData.data);
            }
            
        }
        getBackendData();
    }, [])
    // getBackendData();

    return (
        <div>
            <p>{getData}</p>
        </div>
    )
}