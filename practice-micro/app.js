const express = require('express'); 
var cors = require('cors');
const mongoose = require('mongoose');
  
const app = express(); 
app.use(express.json());
app.use(cors())
const PORT = process.env.EXPRESS_PORT||8500
const uri = process.env.URI||'mongodb://db:27017/mytable'




mongoose.connect(uri, {
 
    useNewUrlParser: true,
    // useCreateIndex: true,
    useUnifiedTopology: true
 
}).then(() => {
    console.log('successfully connected to the database');



}).catch(err => {
    console.log('error connecting to the database');
    console.log(err)
    process.exit();
});



const itemSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: String,
    price: {
        type: String,
        required: true
    }
});

// Model creation
const Item = mongoose.model('Item', itemSchema);




  
app.post('/items-create', async (req, res) => {
    try {
        console.log("incoming: ", req.body);
        const itemData = {
            name: req.body.name,
            description: req.body.description,
            price: req.body.price
        }
        const newItem = new Item(itemData);
        console.log("newItem: ", newItem);

        savedItem = await newItem.save();
        console.log("saved item: ", savedItem);
        res.status(201).send(savedItem);
    } catch (error) {
        console.error("Error during item saving: ", error);
        res.status(500).json({ message: "Error during item saving", error: error.message });
    }
});

app.get('/items-get', async (req, res) => {
    try {
        const items = await Item.find();
        console.log("items in get: ", items)
        res.json(items);
    } catch (error) {
        res.status(500).send(error);
    }
});

app.get('/get-string', (req, res) =>{
    console.log("this is the get string endpoint")
    res.set('Content-Type', 'application/json');
    res.json({ data: 'This is backend content' });
})
app.listen(PORT, (error) =>{ 
    if(!error) 
        console.log("Server is Successfully Running, and App is listening on port "+ PORT) 
    else 
        console.log("Error occurred, server can't start", error); 
    // console.log("running on port "+ PORT)
    } 
); 

